import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CorreoService} from './correo.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Correo, ICorreo } from 'app/shared/model/correo.model';
import {AppResponseBody} from 'app/shared/model/generic-model';
import {AccountService} from 'app/core/auth/account.service';
import {merge, of, Subscription} from 'rxjs';
import {catchError, filter, map, startWith, switchMap} from 'rxjs/operators';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Confirm, MensajeToast} from 'app/mensaje/window.mensaje';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';

@Component({
    selector: 'jhi-correo',
    templateUrl: './correo.component.html'
})
export class CorreoComponent implements OnInit, OnDestroy, AfterViewInit {
    correos: ICorreo[];
    currentAccount: any;
    eventSubscriber: Subscription;
    doNotMatch: string;
    error: string;
    success: string;
    account: any;
    isLoadingResults = false;
    formularioCorreo: FormGroup;
    @ViewChild('formulario', {static: true}) private formDirective: NgForm;

    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    constructor( 
        protected correoService: CorreoService,
        protected accountService: AccountService,
        protected eventManager: JhiEventManager,
        protected snackBar: MatSnackBar,
        protected route: Router
        )
    {
        this.createForm();
    }
    createForm()
    {
            this.formularioCorreo = new FormGroup({
                correo: new FormControl('', [Validators.required, Validators.email]),
                clave: new FormControl('', [Validators.required]),
                host: new FormControl('', [Validators.required]),
                puerto: new FormControl('', [Validators.required]),
                smtp: new FormControl('', [Validators.required]),
                encriptado: new FormControl('', [Validators.required]),
            });
    }
    ngOnInit() {
        this.accountService.identity().subscribe(account => {
            this.account = account;
        });
        this.registerChangeInCorreos();
    }
    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }
    ngAfterViewInit() {}
    loadAll() {
        this.isLoadingResults = true;
        this.correoService
                .findAll()
                .pipe(filter((res: HttpResponse<ICorreo[]>) => res.ok), map((res: HttpResponse<ICorreo[]>) => res.body))
                .subscribe((res: ICorreo[]) => {
                        //this.correos = res;
                        this.correoService.setCorreos(res);
                        this.correos = this.correoService.getCorreos();
                        if(this.correos.length > 0)
                        {
                            this.formularioCorreo = new FormGroup({
                                correo: new FormControl(this.correos[0].correo, [Validators.required, Validators.email]),
                                clave: new FormControl(this.correos[0].clave, [Validators.required]),
                                host: new FormControl(this.correos[0].host, [Validators.required]),
                                puerto: new FormControl(this.correos[0].puerto, [Validators.required]),
                                smtp: new FormControl(this.correos[0].smtp, [Validators.required]),
                                encriptado: new FormControl(this.correos[0].encriptado, [Validators.required]),
                            });
                        }
                        
                    }, (res: HttpErrorResponse) => this.onError(res)
                );
                
    }
    registerChangeInCorreos() {
        this.eventSubscriber = this.eventManager.subscribe('correoListModification', () => this.loadAll());
    }
    enviarDatos() 
    {
        /*
        this.isLoadingResults = false;
        this.error = null;
        this.success = 'OK';
        this.formularioCorreo.reset();

        this.showToast(`Correo guardado correctamente`, 'Información', true);
        */
            
            this.doNotMatch = null;
            this.isLoadingResults = true;

            let correo: Correo = this.formularioCorreo.value;
            if (correo.encriptado) {
                correo.encriptado = '1';
            } 
            else 
            {
                correo.encriptado = '0';
            }
            console.debug("ok", correo.encriptado);
            this.correoService.create(correo).subscribe((res: ICorreo) => {
                        this.isLoadingResults = false;
                        this.error = null;
                        this.success = 'OK';
                        //this.formularioCorreo.reset();
                        this.showToast(`Correo ${correo.correo} guardado correctamente`, 'Información', true);
                    }, (res: HttpErrorResponse) => {
                        this.onError(res);
                        this.isLoadingResults = false;
                        this.success = null;
                        this.error = res.error.error;    
                    });
        }
        
        showToast(mensaje: string, title: string, success: boolean) {
            this.snackBar.openFromComponent(MensajeToast, {
                duration: 5000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
                panelClass: [success ? 'success-snackbar' : 'failure-snackbar'],
                announcementMessage: 'Esto es una prueba',
                data: {description: mensaje, title: title}
            });
        }
        /*
        protected onError(response: HttpErrorResponse) {
            this.isLoadingResults = false;
            this.showToast(response.error.message, 'Error', false);
            if (this.accountService.isAuthenticated()) {
                if (response.status === 401) {
                    this.showToast('Su sesión ha caducado, inicie sesión nuevamente para continuar.', 'Sesión caducada', false);
                    this.route.navigate(['/login']);
                } else if (response.status === 400) {
                    this.showToast('Ocurrió un error. Los datos enviados no son correctos', 'Error', false);
                } else {
                    this.showToast(response.message, 'Error', false);
                }
            }
        }*/
        protected onError(response: HttpErrorResponse) {
            this.isLoadingResults = false;
            if (this.accountService.isAuthenticated()) {
                if (response.status === 401) {
                    this.showToast('Su sesión ha caducado, inicie sesión nuevamente para continuar.', 'Sesión caducada', false);
                    this.route.navigate(['/login']);
                } else if (response.status === 400) {
                    this.showToast('Ocurrió un error. Los datos enviados no son correctos', 'Error', false);
                } else {
                    this.showToast(response.message, 'Error', false);
                }
            }
        }
    }
