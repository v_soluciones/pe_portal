import {Route} from '@angular/router';

import {Injectable} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {UserRouteAccessService} from 'app/core/auth/user-route-access-service';
import {Observable, of} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Correo, ICorreo} from 'app/shared/model/correo.model';
import {CorreoService} from './correo.service';
import {CorreoComponent} from './correo.component';

@Injectable({providedIn: 'root'})
export class CorreoResolve implements Resolve<ICorreo> {

    constructor(private service: CorreoService) {
    }
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICorreo> {
        return of(new Correo());
    }
    
}

export const correoRoute : Routes = [{
    path: '',
    component: CorreoComponent,
    data: {
        authorities: ['ROLE_RECEPTOR', 'ROLE_ADMIN', 'ROLE_USER_EMISOR'],
        pageTitle: 'Configuración de Correo'
    },
    canActivate: [UserRouteAccessService]
    }];
