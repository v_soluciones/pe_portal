import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {SERVER_API_URL} from 'app/app.constants';
import {createRequestOption} from 'app/shared/util/request-util';
import {Correo, ICorreo} from 'app/shared/model/correo.model';
import {AppResponseBody} from 'app/shared/model/generic-model';


type AppResponseArray = AppResponseBody<ICorreo[]>;
type EntityResponseType = HttpResponse<ICorreo>;
type EntityArrayResponseType = HttpResponse<AppResponseArray>;

@Injectable({providedIn: 'root'})
export class CorreoService {
    public resourceUrl = SERVER_API_URL + 'api/correos/create';
    public correoAllResource = SERVER_API_URL + 'api/all/correos';
    private correos: Correo[] = [];
    
    constructor(private http: HttpClient) {
    }
    
    create(correo: ICorreo): Observable<any> {
        return this.http.post(this.resourceUrl, correo, {observe: 'response'});
    }

    findAll(): Observable<HttpResponse<ICorreo[]>> {
        return this.http.get<ICorreo[]>(`${this.correoAllResource}`, {observe: 'response'});
    }
    setCorreos(correos: Correo[]) {
        this.correos = correos;
    }

    getCorreos(): Correo[] {
        return this.correos;
    }
}
