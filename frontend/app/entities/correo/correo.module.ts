import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {PortalSharedModule} from 'app/shared/shared.module';
import {CorreoComponent} from './correo.component';
import {correoRoute} from './correo.route';

const ENTITY_STATES = [...correoRoute];

@NgModule({
    imports: [PortalSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CorreoComponent
    ],
    entryComponents: [CorreoComponent]
})
export class PortalCorreoModule {
}
