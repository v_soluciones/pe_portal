export interface ICorreo {
    id?: number;
    fecha?: Date;
    configuracion?: string;
    correo?: string;
    clave?: string;
    host?: string;
    puerto?: string;
    smtp?: string;
    encriptado?: string;
}

export class Correo implements ICorreo {
    constructor(
        public id?: number,
        public fecha?: Date,
        public configuracion?: string,
        public correo?: string,
        public clave?: string,
        public host?: string,
        public puerto?: string,
        public smtp?: string,
        public encriptado?: string
    ) {
    }
}
