<?php

namespace App\Http\Controllers;

use App\Entity\Documento;
use App\Entity\Usuario;
use App\Entity\FeGeneralConfig;
use App\Entity\UsuarioToken;
use App\Exceptions\GeneralAPIException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Matriphe\Md5Hash\Md5Hasher;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class PHPMailerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws GeneralAPIException
     */
    
    public function sendEmail($idDocumento, $isFromView)
    {
        setlocale(LC_TIME, 'Spanish');
        $fechaActual = Carbon::now();
        $fechaActual = ucfirst($fechaActual->formatLocalized('%B, %Y'));
        $documento = Documento::find($idDocumento);
        $userEmail = env("MAIL_USERNAME", "ylopez@vsperu.com");
        $mail = new PHPMailer(true);
        try {
            $nombreCliente = $documento->cliente->nombreClient;
            $tipoDocumento = $this->findTipoDoc($documento->tipoDoc);
            $estadoDocumento = $this->findEstado($documento->estadoSunat);
            $numeroSerie = $documento->numSerie;
            $view = View::make('phpmail.documento', [
                "rucCliente" => $documento->rucClient,
                "nombreCliente" => $nombreCliente,
                "numSerie" => $numeroSerie,
                "docPdf" => $documento->docPdf,
                "docXml" => $documento->docXml,
                "docCdr" => $documento->docCdr,
                "total" => $documento->total,
                "moneda" => $documento->monedaTransaccion,
                "tipoDocumento" => $tipoDocumento,
                "serieNumero" => $numeroSerie,
                "fechaEmision" => Carbon::parse($documento->fecEmisionDoc)->format(config('mail.emission_date_format')),
                "estadoDocumento" => $estadoDocumento,
                "fecha" => $fechaActual,
            ]);
            $html = $view->render();

            //-$mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = config('mail.host');
            $mail->SMTPAuth = true;
            $mail->Username = config('mail.username');
            $mail->Password = config('mail.password');
            $mail->SMTPSecure = config('mail.encryption');
            $mail->Port = config('mail.port');
            $mail->setFrom($userEmail, config("app.mail_sender_name"));
            $mail->CharSet = 'utf-8';
            //$mail->SMTPKeepAlive = true;
            //-$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Subject = $tipoDocumento . " [$numeroSerie] $estadoDocumento";
            $mail->MsgHTML($html);
            if (isset($documento->email)) {
                Log::info($documento->email);
                $correoCliente = $documento->email;
                $correos = preg_split("/([,;])/", $correoCliente);
                foreach ($correos as $email) {
                    $correoCliente = strtolower(trim($email));
                    break;
                }
                $mail->addAddress($correoCliente);
            } else {
                Log::info($documento->cliente->email);
                $correoCliente = $documento->cliente->email;
                $correos = preg_split("/([,;])/", $correoCliente);
                foreach ($correos as $email) {
                    $correoCliente = strtolower(trim($email));
                    break;
                }
                if(!empty($correoCliente) && $this->validarCorreoRFC2822($correoCliente)){
                    $mail->addAddress($correoCliente);
                }
            }
            $correoProvedor = strtolower(trim(config("mail.username")));
            $emailEmisor = strtolower(trim($documento->emailEmisor));
            //Validacion Email Emisor esta vacio, si es diferente a "-" y al correo de proveedor
            if (!empty($emailEmisor) && $this->validarCorreoRFC2822($emailEmisor) && $emailEmisor != $correoProvedor) {
                //$valemailEmisor = strtolower(trim($emailEmisor));
                $mail->addCC($emailEmisor);
                Log::info("Correo Email Emisor correjido correctamente: ".$emailEmisor);
                Documento::where('idDocumento',$idDocumento)->update(['emailEmisor' => $emailEmisor]);
            }
            $isEnvioCorreoProveedor = env("APP_ENVIO_CORREO_PROVEEDOR", false);
            if ($isEnvioCorreoProveedor) {
                if (!empty($correoProvedor) && $this->validarCorreoRFC2822($correoProvedor))
                    $mail->addCC(strtolower($correoProvedor));
            }
            $emailSecundario = $documento->correoSecundario;
            $emailsValidado = $this->validateEmailSecondary($emailSecundario,$correoProvedor,$emailEmisor);
            $emailC = "";
            foreach($emailsValidado as $key=>$emailV){
                $mail->addCC($emailV);
                if($key == (count($emailsValidado)-1)){
                    $emailC = $emailC.$emailV;
                    Log::info("Correos Secundarios Actualizados: ".$emailC);
                    Documento::where('idDocumento',$idDocumento)->update(['correoSecundario' => $emailC]);
                }else{
                    $emailC = $emailC.$emailV.",";
                }
            }

            $appLogoPath = public_path() . config('app.logo');
            $appLogoCorreoPath = public_path() . config('app.logo_correo');
            $appOkPath = public_path() . config('app.logo_correo');

            $appLinkedinPath = public_path() . '/content/images/linkedin2x.png';
            $appFacebookPath = public_path() . '/content/images/facebook2x.png';
            $appYoutubePath = public_path() . '/content/images/youtube2x.png';

            $mail->addEmbeddedImage($appLogoPath, "app-logo", "Logo Aplicacion");
            $mail->addEmbeddedImage($appLogoCorreoPath, "app-logo-correo", "Logo Correo Aplicacion");
            $mail->addEmbeddedImage($appOkPath, "app-ok", "Fondo Aplicacion");
            $mail->addEmbeddedImage($appLinkedinPath, "linkedin");
            $mail->addEmbeddedImage($appFacebookPath, "facebook");
            $mail->addEmbeddedImage($appYoutubePath, "youtube");
            $typeFilesystem = config("filesystems.default");
            $prefixPath = Storage::disk($typeFilesystem)->getDriver()->getAdapter()->getPathPrefix();

            $docPdf = "";
            if ($documento->tipoTransaccion == 'E') {
                $docPdf = $prefixPath . $documento->docPdf;
            }
            $docXml = $prefixPath . $documento->docXml;
            $docCdr = $prefixPath . $documento->docCdr;
            if ($documento->tipoTransaccion == 'E') {
                $mail->addAttachment($docPdf, $documento->numSerie . '.pdf', PHPMailer::ENCODING_BASE64, 'application/pdf');
            }
            $mail->addAttachment($docXml, $documento->numSerie . '.xml', PHPMailer::ENCODING_BASE64, 'application/vnd.mozilla.xul+xml');
            $mail->addAttachment($docCdr, $documento->numSerie . '.zip', PHPMailer::ENCODING_BASE64, 'application/zip');
            $mail->isSendmail();
            $mail->send();

            $mensajeSatisfactorio = "Se ha enviado el correo exitosamente al cliente: [" . $emailEmisor . ", " . $correoCliente .(empty($emailC) ? "" : ", ".$emailC). "]";
            $mensaje = $isFromView ? $mensajeSatisfactorio : "Documento [" . $documento->numSerie . "] registrado correctamente. " . $mensajeSatisfactorio;
            return response()->json(array("mensaje" => $mensaje), 201);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        }
    }

    public function sendEmailToUser(Usuario $usuario, UsuarioToken $usuarioToken)
    {
        $FeConfig = FeGeneralConfig::find(1);
        $mail = new PHPMailer(true);
        try {

            if($FeConfig["encriptado"]==1) $secure = 'ssl'; else $secure = 'tls';
            $cliente = $usuario->cliente;
            $view = View::make('phpmail.reset-password', [
                "nombreUsuario" => $usuario->nombUsuario,
                "usuarioToken" => $usuarioToken->token,
                "direccion" => $cliente->direccionClient
            ]);
            $html = $view->render();
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->CharSet = 'utf-8';
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = $secure;
            $mail->Host = $FeConfig["host"]; 
            $mail->Port = $FeConfig["puerto"]; 
            $mail->Username = $FeConfig["correo"]; 
            $mail->Password = $FeConfig["secret"]; 
            $mail->setFrom($FeConfig["correo"], config("app.mail_sender_name"));
            $mail->SMTPKeepAlive = true;
            $mail->Subject = 'Reinicio de Contraseña de Usuario - ' . $usuario->nombUsuario;
            $mail->MsgHTML($html);
            $mail->addAddress($usuario->email);

            $appLogoPath = public_path() . config('app.logo');
            $appOkPath = public_path() . '/content/images/illo.png';
            $appFondoPath = public_path() . '/content/images/bg_password.gif';
            $appFacebookPath = public_path() . '/content/images/facebook2x.png';
            $appTwitterPath = public_path() . '/content/images/twitter2x.png';
            $appGooglePath = public_path() . '/content/images/googleplus2x.png';
            $mail->addEmbeddedImage($appLogoPath, "logo-aplicacion", "Logo Aplicacion");
            $mail->addEmbeddedImage($appOkPath, "fondo-aplicacion", "Fondo Aplicacion");
            $mail->addEmbeddedImage($appFacebookPath, "facebook", "Facebook");
            $mail->addEmbeddedImage($appTwitterPath, "twitter", "Twitter");
            $mail->addEmbeddedImage($appGooglePath, "google", "Google");
            $mail->addEmbeddedImage($appFondoPath, "logo-fondo", "Fondo Imagen");

            $mail->send();
            return response()->json(array("mensaje" => "Se ha enviado el correo exitosamente a: " . $usuario->email));
        } catch (Exception $e) {
            return response()->json(array("error" => $e->errorMessage()), 500);
        }
    }

    public function sendRegisterEmail(Usuario $usuario, $password)
    {
        $mail = new PHPMailer(true);
        try {
            $cliente = $usuario->cliente;
            $view = View::make('phpmail.user-register', [
                "nombreUsuario" => $usuario->nombUsuario,
                "password" => $password,
                "direccion" => $cliente->direccionClient,
            ]);
            $html = $view->render();

            $userEmail = env("MAIL_USERNAME", "hfigueroa@vsperu.com");
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = config('mail.host');
            $mail->SMTPAuth = true;
            $mail->Username = config('mail.username');
            $mail->Password = config('mail.password');
            $mail->SMTPSecure = config('mail.encryption');
            $mail->Port = config('mail.port');
            $mail->setFrom($userEmail, config("app.mail_sender_name"));
            $mail->CharSet = 'utf-8';

            $mail->Subject = config('app.name') . ' te ha enviado el registro de un nuevo usuario - ' . $usuario->nombUsuario;
            $mail->MsgHTML($html);

            $correoCliente = $usuario->email;
            $correos = preg_split("/([,;])/", $correoCliente);
            foreach ($correos as $email) {
                $correoCliente = $email;
                if(!empty($correoCliente) && $this->validarCorreoRFC2822($correoCliente)){
                    $mail->addAddress($correoCliente);
                }else{
                    return response()->json(array("error" => "Correo electronico del cliente no cumple con el formato RFC 2822"), 500);
                    break;
                }
            }
            
            $appLogoPath = public_path() . config('app.logo');
            $appOkPath = public_path() . '/content/images/illo.png';
            $appbackPath = public_path() . '/content/img/background.png';
            $appLinkedinPath = public_path() . '/content/images/linkedin2x.png';
            $appFacebookPath = public_path() . '/content/images/facebook2x.png';
            $appYoutubePath = public_path() . '/content/images/youtube2x.png';
            
            $mail->addEmbeddedImage($appLogoPath, "logo-aplicacion");
            $mail->addEmbeddedImage($appOkPath, "fondo-aplicacion");
            $mail->addEmbeddedImage($appbackPath, "logo-fondo");
            $mail->addEmbeddedImage($appLinkedinPath, "linkedin");
            $mail->addEmbeddedImage($appFacebookPath, "facebook");
            $mail->addEmbeddedImage($appYoutubePath, "youtube");
            
            $mail->send();
            return response()->json(array("mensaje" => "Se ha enviado el correo exitosamente a: " . $usuario->email));
        } catch (Exception $e) {
            return response()->json(array("error" => $e->errorMessage()), 500);
        }
    }

    public function sendRestorePasswordEmail(Usuario $usuario, $password)
    {
        $FeConfig = FeGeneralConfig::find(1);
        $mail = new PHPMailer(true);
        try {
            if($FeConfig["encriptado"]==1) $secure = 'ssl'; else $secure = 'tls';
            $nombreUsuario = $usuario->nombUsuario;
            $email = $usuario->email;
            
            $view = View::make('phpmail.restore-password', [
                "nombreUsuario" => $nombreUsuario,
                "password" => $password,
                "direccion" => config('app.name')
            ]);
            $html = $view->render();
            $mail->isSMTP();
            $mail->SMTPDebug = 2;
            $mail->CharSet = 'utf-8';
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = $secure;
            $mail->Host = $FeConfig["host"]; 
            $mail->Port = $FeConfig["puerto"]; 
            $mail->Username = $FeConfig["correo"]; 
            $mail->Password = $FeConfig["secret"]; 
            $mail->setFrom($FeConfig["correo"] , config('app.mail_sender_name'));
            $mail->SMTPKeepAlive = true;
            $mail->Subject = 'Reinicio de Contraseña de Usuario - ' . $usuario->nombUsuario;
            $mail->MsgHTML($html);
            $mail->addAddress($email);

            $appLogoPath = public_path() . config('app.logo');
            $appOkPath = public_path() . '/content/images/illo.png';
            $appFondoPath = public_path() . '/content/images/bg_password.gif';
            $appFacebookPath = public_path() . '/content/images/facebook2x.png';
            $appTwitterPath = public_path() . '/content/images/twitter2x.png';
            $appGooglePath = public_path() . '/content/images/googleplus2x.png';
            $mail->addEmbeddedImage($appLogoPath, "logo-aplicacion", "Logo Aplicacion");
            $mail->addEmbeddedImage($appOkPath, "fondo-aplicacion", "Fondo Aplicacion");
            $mail->addEmbeddedImage($appFacebookPath, "facebook", "Facebook");
            $mail->addEmbeddedImage($appTwitterPath, "twitter", "Twitter");
            $mail->addEmbeddedImage($appGooglePath, "google", "Google");
            $mail->addEmbeddedImage($appFondoPath, "logo-fondo", "Fondo Imagen");

            //$mail->isSendmail();
            $mail->send();
            return response()->json(array("mensaje" => "Se ha enviado el correo exitosamente a: " . $email));
        } catch (Exception $e) {
            return response()->json(array("error" => $e->errorMessage()), 500);
        }
    }

    private function findTipoDoc($tipo = "factura")
    {
        switch ($tipo) {
            case "01":
                return "Factura de Venta";
            case "03":
                return "Boleta de Venta";
            case "07":
                return "Nota de Crédito";
            case "08":
                return "Nota de Débito";
            case "40":
                return "Comprobante Percepción";
            case "20":
                return "Comprobante Retencion";
            case "99":
                return "Baja Aprobada";
            case "09":
                return "Guía Remisión";
            case "RC":
                return "Resumen Diario de Boleta";
            default:
                return "01";
        }
    }

    private function findEstado($estado = "R")
    {
        switch ($estado) {
            case 'R':
            case 'V':
                return 'Aprobado';
            case 'Z':
                return 'Rechazado';
            case 'D':
                return 'Pendiente Respuesta';
            case 'P':
                return 'Baja Aprobada';
            case 'J':
                return 'Baja Rechazada';
        }
    }

    public function validateEmailSecondary($emailSecundario,$correoProvedor,$emailEmisor)
    {
        $array = array();
        if (!empty($emailSecundario) && $emailSecundario != "-") {
            $correos = preg_split("/([,;])/", $emailSecundario);
            foreach ($correos as $correo) {
                $correov = strtolower(trim($correo));
                if (preg_match("/[a-zA-Z]/", $correov) && $this->validarCorreoRFC2822($correov)){
                    if($correov != $correoProvedor && $correov != $emailEmisor){
                        array_push($array,strtolower($correov));
                    }
                }
            }
        }
        return $array;
    }
    function validarCorreoRFC2822($correo) {
        // Patrón de expresión regular para validar una dirección de correo electrónico según RFC 2822
        $patron = '/^(?!(?:(?:\r\n)?[ \t])|(?:\r\n.{0,3}[ \t]))[^\s@<>,;:\\"[]()]+(?:\.[^\s@<>,;:\\"[]()]+)*@(?:[^\s@<>,;:\\"[]()]+\.)+[^\s@<>,;:\\"[]()]+$/';
        
        //if (preg_match($patron, $correo)) {
        if(filter_var($correo, FILTER_VALIDATE_EMAIL)){
            return true;
        } else {
            return false;
        }
    }
    /*
    | Recover Password
    |----------------------------------------------
    | Method created for the recovery of the User's password. 
    | It can be retrieved through your username or Ruc. An email 
    | will be sent to you with your new random password.
    */
    public function sendPassEmail(Usuario $usuario, $pass){

        $userEmail = env("MAIL_USERNAME", "ylopez@vsperu.com");
        $mail = new PHPMailer(true);
        try {
            $emailUsuario = $usuario->email;
            $nombCliente = $usuario->cliente->nombreClient;
            $nombUsuario = $usuario->nombUsuario;
            $view = View::make('emails.restore-password-email', [
                "nombreCliente" => $nombCliente,
                "nombreUsuario" => $nombUsuario,
                "password" => $pass,
            ]);
            $html = $view->render();

            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = config('mail.host');
            $mail->SMTPAuth = true;
            $mail->Username = config('mail.username');
            $mail->Password = config('mail.password');
            $mail->SMTPSecure = config('mail.encryption');
            $mail->Port = config('mail.port');
            $mail->setFrom($userEmail, config("app.mail_sender_name"));
            $mail->CharSet = 'utf-8';
            $mail->Subject = config('app.mail_sender_name').": Usuario y Contraseña";
            $mail->MsgHTML($html);
            $mail->addAddress($emailUsuario);

            $mail->send();

            $mensaje = "Se ha enviado exitosamente a: ".$emailUsuario;
            return response()->json(array("success" => true, "msg" => $mensaje),200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw $e;
            return response()->json(array("success" => false, "error" => $e), 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw $e;
            return response()->json(array("success" => false, "error" => $e), 404);
        }
    }
}
