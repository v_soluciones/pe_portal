<?php

namespace App\Http\Controllers;

use App\Entity\Documento;
use App\Entity\Usuario;
use App\Entity\UsuarioToken;
use App\Exceptions\GeneralAPIException;
use App\Libs\GmailAPI\Mailer;
use Carbon\Carbon;
use Dacastro4\LaravelGmail\Facade\LaravelGmail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PHPMailer\PHPMailer\Exception;

class GmailMailerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws GeneralAPIException
     */
    public function sendEmail(Documento $documento, $isFromView, $idDocumento)
    {
        $this->resolveTokenIfExpired();
        $fechaActual = $this->getMonth();
        $userEmail = env("MAIL_USERNAME", "ylopez@vsperu.com");
        $mail = new Mailer();
        try {
            $nombreCliente = $documento->cliente->nombreClient;
            $tipoDocumento = $this->findTipoDoc($documento->tipoDoc);
            $estadoDocumento = $this->findEstado($documento->estadoSunat);
            $numeroSerie = $documento->numSerie;
            $dataArray = [
                "rucCliente" => $documento->rucClient,
                "nombreCliente" => $nombreCliente,
                "numSerie" => $numeroSerie,
                "docPdf" => $documento->docPdf,
                "docXml" => $documento->docXml,
                "docCdr" => $documento->docCdr,
                "total" => $documento->total,
                "moneda" => $documento->monedaTransaccion,
                "tipoDocumento" => $tipoDocumento,
                "serieNumero" => $numeroSerie,
                "fechaEmision" => Carbon::parse($documento->fecEmisionDoc)->format(config('mail.emission_date_format')),
                "estadoDocumento" => $estadoDocumento,
                "fecha" => $fechaActual,
            ];
            $mail->view('phpmail.documento', $dataArray);
            //$mail->from($userEmail, config("app.mail_sender_name"));
            $mail->subject($tipoDocumento . " [$numeroSerie] $estadoDocumento");
            if (isset($documento->email)) {
                Log::info($documento->email);
                $correoCliente = $documento->email;
                $correos = preg_split("/([,;])/", $correoCliente);
                foreach ($correos as $email) {
                    $correoCliente = strtolower(trim($email));
                    break;
                }
                if(!empty($correoCliente) && $this->validarCorreoRFC2822($correoCliente)){
                    $mail->to($correoCliente);
                }
            } else {
                Log::info($documento->cliente->email);
                $correoCliente = $documento->cliente->email;
                $correos = preg_split("/([,;])/", $correoCliente);
                foreach ($correos as $email) {
                    $correoCliente = strtolower(trim($email));
                    break;
                }
                if(!empty($correoCliente) && $this->validarCorreoRFC2822($correoCliente)){
                    $mail->to($correoCliente);
                }
            }
            $correoProveedor = strtolower(trim(config("mail.username")));
            $emailEmisor = strtolower(trim($documento->emailEmisor));
            $emailSecundario = strtolower($documento->correoSecundario);
            $emailC = "";
            $ccEmails = array();

            if (!empty($emailEmisor) && $this->validarCorreoRFC2822($emailEmisor) && $emailEmisor != $correoProveedor) {
                array_push($ccEmails,$emailEmisor);
                Log::info("Correo Email Emisor correjido correctamente: ".$emailEmisor);
                Documento::where('idDocumento',$idDocumento)->update(['emailEmisor' => $emailEmisor]);
            }
            $isEnvioCorreoProveedor = env("APP_ENVIO_CORREO_PROVEEDOR", false);
            if ($isEnvioCorreoProveedor) {
                if (!empty($correoProveedor) && $this->validarCorreoRFC2822($correoProveedor))
                    array_push($ccEmails,$correoProveedor);
            }
            
            $emailsValidado = $this->validateEmailSecondary($emailSecundario,$correoProveedor,$emailEmisor);

            foreach($emailsValidado as $key=>$emailV){
                if($key == (count($emailsValidado)-1)){
                    $emailC = $emailC.$emailV;
                    Log::info("Correos Secundarios Actualizados: ".$emailC);
                    Documento::where('idDocumento',$idDocumento)->update(['correoSecundario' => $emailC]);
                }else{
                    $emailC = $emailC.$emailV.",";
                }
            }
            $ccEmails = array_merge($ccEmails, $emailsValidado);
            
            if (!empty($ccEmails)) {
                $mail->cc($ccEmails);
            }
            $appLogoPath = public_path() . config('app.logo');
            $appLogoCorreoPath = public_path() . config('app.logo_correo');
            $appLinkedinPath = public_path() . '/content/images/linkedin2x.png';
            $appFacebookPath = public_path() . '/content/images/facebook2x.png';
            $appYoutubePath = public_path() . '/content/images/youtube2x.png';
            $mail->addEmbeddedImage($appLogoPath, "app-logo");
            $mail->addEmbeddedImage($appLogoCorreoPath, "app-logo-correo");
            $mail->addEmbeddedImage($appLinkedinPath, "linkedin");
            $mail->addEmbeddedImage($appFacebookPath, "facebook");
            $mail->addEmbeddedImage($appYoutubePath, "youtube");
            
            $typeFilesystem = config("filesystems.default");
            $prefixPath = Storage::disk($typeFilesystem)->getDriver()->getAdapter()->getPathPrefix();

            $docPdf = "";
            if ($documento->tipoTransaccion == 'E') {
                $docPdf = $prefixPath . $documento->docPdf;
            }
            $docXml = $prefixPath . $documento->docXml;
            $docCdr = $prefixPath . $documento->docCdr;
            if ($documento->tipoTransaccion == 'E') {
                $mail->attach($docPdf);
            }
            $mail->attach($docXml);
            $mail->attach($docCdr);
            $mail->send();

            $correosSecundarios = preg_split("/([,;])/", $emailSecundario);
            $correos = join(", ", $correosSecundarios);
            //(empty($emailSecundario) ? "" : ", " . $correos) 
            $mensajeSatisfactorio = "Se ha enviado el correo exitosamente al cliente: [" . $emailEmisor . ", " . $correoCliente .(empty($emailC) ? "" : ", ".$emailC). "]";
            $mensaje = $isFromView ? $mensajeSatisfactorio : "Documento [" . $documento->numSerie . "] registrado correctamente. " . $mensajeSatisfactorio;
            return response()->json(array("mensaje" => $mensaje), 201);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        }
    }

    public function sendEmailToUser(Usuario $usuario, UsuarioToken $usuarioToken)
    {
        $this->resolveTokenIfExpired();
        $userEmail = env("MAIL_USERNAME", "ylopez@vsperu.com");
        $mail = new Mailer();
        try {
            $cliente = $usuario->cliente;
            $dataArray = [
                "nombreUsuario" => $usuario->nombUsuario,
                "usuarioToken" => $usuarioToken->token,
                "direccion" => $cliente->direccionClient
            ];
            $mail->view('phpmail.reset-password', $dataArray);
            $mail->from($userEmail, config("app.mail_sender_name"));
            $mail->subject('Reinicio de Contraseña de Usuario - ' . $usuario->nombUsuario);
            $mail->to($usuario->email);
            $appLogoPath = public_path() . config('app.logo');
            $appOkPath = public_path() . '/content/images/illo.png';
            $appFondoPath = public_path() . '/content/images/bg_password.gif';
            $appFacebookPath = public_path() . '/content/images/facebook2x.png';
            $appTwitterPath = public_path() . '/content/images/facebook2x.png';
            $appGooglePath = public_path() . '/content/images/facebook2x.png';
            $mail->addEmbeddedImage($appLogoPath, "app-logo");
            $mail->addEmbeddedImage($appLinkedinPath, "linkedin");
            $mail->addEmbeddedImage($appFacebookPath, "facebook");
            $mail->addEmbeddedImage($appYoutubePath, "youtube");
            $mail->addEmbeddedImage($appFondoPath, "logo-fondo");
            $mail->send();
            return response()->json(array("mensaje" => "Se ha enviado el correo exitosamente a: " . $userEmail));
        } catch (Exception $e) {
            return response()->json(array("error" => $e->errorMessage()), 500);
        }
    }

    public function sendRegisterEmail(Usuario $usuario, $password)
    {
        $this->resolveTokenIfExpired();
        //$userEmail = env("MAIL_USERNAME", "ylopez@vsperu.com");
        $mail = new Mailer();
        try {
                $cliente = $usuario->cliente;
                $dataArray = [
                    "nombreUsuario" => $usuario->nombUsuario,
                    "password" => $password,
                    "direccion" => $cliente->direccionClient,
                ];
                $mail->view('phpmail.user-register', $dataArray);
                //$mail->from($userEmail, config('app.mail_sender_name'));
                $mail->subject(config('app.name') . ' te ha enviado el registro de un nuevo usuario - ' . $usuario->nombUsuario);
                $correoCliente = $usuario->email;
                $correos = preg_split("/([,;])/", $correoCliente);
                foreach ($correos as $email) {
                    $correoCliente = $email;
                    if(!empty($correoCliente) && $this->validarCorreoRFC2822($correoCliente)){
                        $mail->to($usuario->email);
                    }else{
                        return response()->json(array("error" => "Correo electronico del cliente no cumple con el formato RFC 2822"), 500);
                        break;
                    }
                }
                
                $appLogoPath = public_path() . config('app.logo');
                $appOkPath = public_path() . '/content/images/illo.png';
                $appbackPath = public_path() . '/content/img/background.png';
                $appLinkedinPath = public_path() . '/content/images/linkedin2x.png';
                $appFacebookPath = public_path() . '/content/images/facebook2x.png';
                $appYoutubePath = public_path() . '/content/images/youtube2x.png';
                
                $mail->addEmbeddedImage($appLogoPath, "logo-aplicacion");
                $mail->addEmbeddedImage($appOkPath, "fondo-aplicacion");
                $mail->addEmbeddedImage($appbackPath, "logo-fondo");
                $mail->addEmbeddedImage($appLinkedinPath, "linkedin");
                $mail->addEmbeddedImage($appFacebookPath, "facebook");
                $mail->addEmbeddedImage($appYoutubePath, "youtube");
                
                $mail->send();
                return response()->json(array("mensaje" => "Se ha enviado el correo exitosamente a: " . $userEmail));
            
        } catch (Exception $e) {
            return response()->json(array("error" => $e->errorMessage()), 500);
        }
    }

    public function sendRestorePasswordEmail(Usuario $usuario, $password)
    {
        $this->resolveTokenIfExpired();
        $userEmail = env("MAIL_USERNAME", "ylopez@vsperu.com");
        $mail = new Mailer();
        try {
            $nombreUsuario = $usuario->nombUsuario;
            $email = $usuario->email;
            $dataArray = [
                "nombreUsuario" => $nombreUsuario,
                "password" => $password,
                "direccion" => config('app.name')
            ];
            $mail->from($userEmail, config('app.mail_sender_name'));
            $mail->subject('Reinicio de Contraseña de Usuario - ' . $usuario->nombUsuario);
            $mail->to($email);
            $mail->view('phpmail.restore-password', $dataArray);
            $appLogoPath = public_path() . config('app.logo');
            $appOkPath = public_path() . '/content/images/illo.png';
            $appFondoPath = public_path() . '/content/images/bg_password.gif';
            $appFacebookPath = public_path() . '/content/images/facebook2x.png';
            $appTwitterPath = public_path() . '/content/images/facebook2x.png';
            $appGooglePath = public_path() . '/content/images/facebook2x.png';
            $mail->addEmbeddedImage($appLogoPath, "app-logo");
            $mail->addEmbeddedImage($appLinkedinPath, "linkedin");
            $mail->addEmbeddedImage($appFacebookPath, "facebook");
            $mail->addEmbeddedImage($appYoutubePath, "youtube");
            $mail->addEmbeddedImage($appFondoPath, "logo-fondo");
            $mail->send();
            return response()->json(array("mensaje" => "Se ha enviado el correo exitosamente a: " . $email));
        } catch (Exception $e) {
            return response()->json(array("error" => $e->errorMessage()), 500);
        }
    }

    private function resolveTokenIfExpired()
    {
        $check = LaravelGmail::check();
        if (!$check) {
            $refreshToken = env("GOOGLE_REFRESH_TOKEN", "");
            LaravelGmail::fetchAccessTokenWithRefreshToken($refreshToken);
            $token = LaravelGmail::getAccessToken();
            LaravelGmail::setBothAccessToken($token);
        }
    }

    private function findTipoDoc($tipo = "factura")
    {
        switch ($tipo) {
            case "01":
                return "Factura de Venta";
            case "03":
                return "Boleta de Venta";
            case "07":
                return "Nota de Crédito";
            case "08":
                return "Nota de Débito";
            case "40":
                return "Comprobante Percepción";
            case "20":
                return "Comprobante Retencion";
            case "09":
                return "Guía Remisión";
            case "RC":
                return "Resumen Diario de Boleta";
            default:
                return "01";
        }
    }

    private function findEstado($estado = "R")
    {
        switch ($estado) {
            case 'R':
            case 'V':
                return 'Aprobado';
            case 'Z':
                return 'Rechazado';
            case 'D':
                return 'Pendiente Respuesta';
            case 'P':
                return 'Baja Aprobada';
            case 'J':
                return 'Baja Rechazada';
        }
    }

    private function getMonth()
    {
        $now = Carbon::now();
        $mes = $now->month;
        switch ($mes) {
            case 1:
                $mes = 'Enero';
                break;
            case 2:
                $mes = 'Febrero';
                break;
            case 3:
                $mes = 'Marzo';
                break;
            case 4:
                $mes = 'Abril';
                break;
            case 5:
                $mes = 'Mayo';
                break;
            case 6:
                $mes = 'Junio';
                break;
            case 7:
                $mes = 'Julio';
                break;
            case 8:
                $mes = 'Agosto';
                break;
            case 9:
                $mes = 'Septiembre';
                break;
            case 10:
                $mes = 'Octubre';
                break;
            case 11:
                $mes = 'Noviembre';
                break;
            case 12:
                $mes = 'Diciembre';
                break;
        }
        return "" . $mes . " " . $now->day . ", " . $now->year;
    }

    public function validateEmailSecondary($emailSecundario,$correoProvedor,$emailEmisor)
    {
        $array = array();
        if (!empty($emailSecundario) && $emailSecundario != "-") {
            $correos = preg_split("/([,;])/", $emailSecundario);
            foreach ($correos as $correo) {
                $correov = trim($correo);
                if (preg_match("/[a-zA-Z]/", $correov) && $this->validarCorreoRFC2822($correov)){
                    if(strtolower($correov) != strtolower($correoProvedor) && strtolower($correov) != strtolower($emailEmisor)){
                        array_push($array,strtolower($correov));
                    }
                }
            }
        }
        return $array;
    }

    function validarCorreoRFC2822($correo) {
        // Patrón de expresión regular para validar una dirección de correo electrónico según RFC 2822
        $patron = '/^(?!(?:(?:\r\n)?[ \t])|(?:\r\n.{0,3}[ \t]))[^\s@<>,;:\\"[]()]+(?:\.[^\s@<>,;:\\"[]()]+)*@(?:[^\s@<>,;:\\"[]()]+\.)+[^\s@<>,;:\\"[]()]+$/';
        //if (preg_match($patron, $correo)) {
        if(filter_var($correo, FILTER_VALIDATE_EMAIL)){
            return true;
        } else {
            return false;
        }
    }

    /*
    | Recover Password
    |----------------------------------------------
    | Method created for the recovery of the User's password. 
    | It can be retrieved through your username or Ruc. An email 
    | will be sent to you with your new random password.
    */
    public function sendPassEmail(Usuario $usuario, $pass){
        $this->resolveTokenIfExpired();

        $userEmail = env("MAIL_USERNAME", "ylopez@vsperu.com");
        $mail = new Mailer();
        try {
            $emailUsuario = $usuario->email;
            $nombCliente = $usuario->cliente->nombreClient;
            $nombUsuario = $usuario->nombUsuario;
            $dataArray = [
                "nombreCliente" => $nombCliente,
                "nombreUsuario" => $nombUsuario,
                "password" => $pass,
            ];
            $mail->view('emails.restore-password-email', $dataArray);

            $mail->subject("Ventura Soluciones: Usuario y Contraseña");
            $mail->to($emailUsuario);
            $mail->send();

            $mensaje = "Se ha enviado exitosamente a: ".$emailUsuario;
            return response()->json(array("success" => true, "msg" => $mensaje),200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw $e;
            return response()->json(array("success" => false, "error" => $e), 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw $e;
            return response()->json(array("success" => false, "error" => $e), 404);
        }
    }
}
