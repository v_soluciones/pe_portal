<?php


namespace App\Http\Controllers;

use App\Entity\Cliente;
use App\Entity\Documento;
use App\Entity\Usuario;
use App\Exceptions\GeneralAPIException;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Matriphe\Md5Hash\Md5Hash;
use Matriphe\Md5Hash\Md5Hasher;
use PDO;
use Tymon\JWTAuth\Facades\JWTAuth;

class PublicadorController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function publicar(Request $request)
    {
        $serieNumero = "";
        $documentoRegistrado = false;
        try {
            $hasher = new Md5Hasher();
            $credentials = array("password" => $hasher->make($request->claveSesion), "nombUsuario" => $request->usuarioSesion);
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Las credenciales proporcionadas para el servicio no son correctas'], 401);
            }
            Log::info("api: ".json_encode($request->all()));
            $dataCliente = $request->only(['direccionClient', 'email', 'estadoCliente', 'nombreClient', 'rucClient', 'rutaImagenClient']);
            $dataCliente["email"] = strtolower(trim($dataCliente["email"]));//Restructuracion de formato de escritura
            $datosCliente = array();
            foreach ($dataCliente as $key => $value) {
                $datosCliente[] = $key . "=>" . $value;
            }
            Log::info('Recibiendo los datos del cliente: [' . join(", ", $datosCliente) . "]");
            $clienteDb = Cliente::find($dataCliente["rucClient"]);
            if (!isset($clienteDb)) {
                $cliente = new Cliente();
                $dataCliente["estadoCliente"] = 1;
                $cliente->fill($dataCliente)->save();
                $this->registerUser($cliente, $dataCliente["rucClient"]);
                Log::info('Cliente registrado correctamente');
            } else {
                $dataCliente["estadoCliente"] = 1;
                $clienteDb->fill($dataCliente)->update();
            }
            try {
                $usuarioDb = Usuario::where("rucClient", $dataCliente["rucClient"])->first();
                if (isset($usuarioDb)) {
                    $dataUsuario = array();
                    $dataUsuario["email"] = strtolower(trim($dataCliente["email"]));
                    $usuarioDb->fill($dataUsuario)->update();
                    Log::info('Correo del usuario modificado correctamente.');
                }
            } catch (Exception $e) {
                Log::error("Ocurrio un problema al buscar el usuario.");
                Log::error($e->getMessage());
            }
            $mensajeErrorAnexo = false;
            $documento = new Documento();
            $data = $request->only(["numSerie", "fecEmisionDoc","created_at", 'estadoSunat', 'estadoWeb', "correoSecundario", 'tipoDoc', "tipoTransaccion", "total", "docPdf",
                "docXml", "docCdr", "rucClient", "rsRuc", "monedaTransaccion", "emailEmisor", "serie"]);
            $data["emailEmisor"] = strtolower(trim($data["emailEmisor"]));//Restructuracion de formato de escritura
            $data["correoSecundario"] = strtolower($data["correoSecundario"]);//Restructuracion de formato de escritura
            try {
                $timeData = $request->only(["start_at", "end_at"]);
                $data["start_at"] = Carbon::createFromFormat("H:i:s", $timeData["start_at"]);
                $data["end_at"] = Carbon::createFromFormat("H:i:s", $timeData["end_at"]);
            } catch (Exception $e) {
                Log::error("No se encuentra el tiempo en el documento se creara con el tiempo actual.");
                Log::error($e->getMessage());
                $ahora = Carbon::now();
                $data["start_at"] = $ahora;
                $data["end_at"] = $ahora->addMinute();
            }
            $serieNumero = $data["numSerie"];
            $data["idDocumento"] = $this->getLastIdFromTable();
            $data["estadoWeb"] = "P";
            $fechaEmisionDocumento = $data["fecEmisionDoc"];
            $data["fecEmisionDoc"] = Carbon::createFromFormat("d/m/Y", $data["fecEmisionDoc"]);
            $docPdf = $data["docPdf"];
            $docXML = $data["docXml"];
            $docCdr = $data["docCdr"];
            $fileName = $data["numSerie"];
            try {
                $filePdf = base64_decode($docPdf);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en el documento Impreso" . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            try {
                $fileXml = base64_decode($docXML);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en el archivo XML" . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            try {
                $fileCdr = base64_decode($docCdr);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en la Respuesta CDR." . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            $fechaEmision = explode("/", $fechaEmisionDocumento);
            $dateDirectory = join(DIRECTORY_SEPARATOR, array_reverse($fechaEmision));
            $directory = join(DIRECTORY_SEPARATOR, array($dateDirectory, $data["rucClient"]));
            Storage::makeDirectory($directory);
            $localPath = join(DIRECTORY_SEPARATOR, array($dateDirectory, $data["rucClient"], $fileName));
            $typeFilesystem = config("filesystems.default");
            
            if($data['tipoTransaccion']!='B') Storage::disk($typeFilesystem)->put($localPath . '.pdf', $filePdf);
            Storage::disk($typeFilesystem)->put($localPath . '.xml', $fileXml);
            Storage::disk($typeFilesystem)->put($localPath . '.zip', $fileCdr);
            $data["docPdf"] = $localPath . '.pdf';
            $data["docXml"] = $localPath . '.xml';
            $data["docCdr"] = $localPath . '.zip';
            $datosDocumento = array();
            foreach ($data as $key => $value) {
                $datosDocumento[] = $key . "=>" . $value;
            }
            Log::info('Recibiendo los datos del documento: [' . join(", ", $datosDocumento) . "]");
            $documentoDb = Documento::where("numSerie", $data["numSerie"])->first();
            $token = openssl_random_pseudo_bytes(64);
            $token = bin2hex($token);
            Log::info('Guardando el formato impreso en: ' . $data["docPdf"]);
            Log::info('Guardando el XML del Documento en: ' . $data["docXml"]);
            Log::info('Guardando la Respuesta CDR en: ' . $data["docCdr"]);
            if ($documentoDb && $data['tipoTransaccion']!='B') {
                $data["idDocumento"] = $documentoDb->idDocumento;
                $data["token"] = $token;
                $documentoDb->fill($data)->update();
                $documentoRegistrado = true;
                $documento = $documentoDb;
            } else {
                $data["token"] = $token;
                $documento->fill($data)->save();
                $documentoRegistrado = true;
            }
            Log::info('');
            $documentoAQ = Documento::where("numSerie", $data["numSerie"])->get();
            //$usePHPMailer = config('app.use_phpmailer');
            $usePHPMailer = env("USE_PHPMAILER", false);
            $useGmailApi = env("USE_GMAIL_API", false);
            //dd($usePHPMailer);
            if ($useGmailApi) {
                $controller = new GmailMailerController();
                return $controller->sendEmail($documento, false, $data["idDocumento"]);
            } else if ($usePHPMailer) {
                $documentoController = new PHPMailerController();
                return $documentoController->sendEmail($data["idDocumento"], false);
            } else {
                $documentoController = new EmailController();
                return $documentoController->sendEmail($data["idDocumento"], false);
            }
        } catch (Exception $e) {
            Log::error($e->getTraceAsString());
            if ($e instanceof GeneralAPIException) {
                if ($documentoRegistrado) {
                    return response()->json(array("mensaje" => "Se registró existosamente el documento [" . $serieNumero . "] pero: " . $e->getMessage()), 201);
                } else {
                    return response()->json(array("mensaje" => "Error en el documento [" . $serieNumero . "]: " . $e->getMessage()), 400);
                }
            }
            if ($e instanceof Exception) {
                return response()->json(array("mensaje" => " No se pudo registar el documento [" . $serieNumero . "] por: " . $e->getMessage()), 201);
            }
            if (!$mensajeErrorAnexo) {
                return response()->json(array("mensaje" => "No se pudo registar el documento [" . $serieNumero . "] por: " . $mensajeErrorAnexo), 201);
            }
            return response()->json(array("mensaje" => $e->getCode(), "error" => $e->getMessage(), "archivo" => $e->getFile()), 500);
        }
    }
    //Publicar sin enviar email
    public function publishOnly(Request $request){
        $serieNumero = "";
        $documentoRegistrado = false;
        try {
            $hasher = new Md5Hasher();
            $credentials = array("password" => $hasher->make($request->claveSesion), "nombUsuario" => $request->usuarioSesion);
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Las credenciales proporcionadas para el servicio no son correctas'], 401);
            }
            Log::info("api: ".json_encode($request->all()));
            $dataCliente = $request->only(['direccionClient', 'email', 'estadoCliente', 'nombreClient', 'rucClient', 'rutaImagenClient']);
            $dataCliente["email"] = strtolower(trim($dataCliente["email"]));//Restructuracion de formato de escritura
            $datosCliente = array();
            foreach ($dataCliente as $key => $value) {
                $datosCliente[] = $key . "=>" . $value;
            }
            Log::info('Recibiendo los datos del cliente: [' . join(", ", $datosCliente) . "]");
            $clienteDb = Cliente::find($dataCliente["rucClient"]);
            if (!isset($clienteDb)) {
                $cliente = new Cliente();
                $dataCliente["estadoCliente"] = 1;
                $cliente->fill($dataCliente)->save();
                $this->registerUser($cliente, $dataCliente["rucClient"]);
                Log::info('Cliente registrado correctamente');
            } else {
                $dataCliente["estadoCliente"] = 1;
                $clienteDb->fill($dataCliente)->update();
            }
            try {
                $usuarioDb = Usuario::where("rucClient", $dataCliente["rucClient"])->first();
                if (isset($usuarioDb)) {
                    $dataUsuario = array();
                    $dataUsuario["email"] = strtolower(trim($dataCliente["email"]));
                    $usuarioDb->fill($dataUsuario)->update();
                    Log::info('Correo del usuario modificado correctamente.');
                }
            } catch (Exception $e) {
                Log::error("Ocurrio un problema al buscar el usuario.");
                Log::error($e->getMessage());
            }
            $mensajeErrorAnexo = false;
            $documento = new Documento();
            $data = $request->only(["numSerie", "fecEmisionDoc","created_at", 'estadoSunat', 'estadoWeb', "correoSecundario", 'tipoDoc', "tipoTransaccion", "total", "docPdf",
                "docXml", "docCdr", "rucClient", "rsRuc", "monedaTransaccion", "emailEmisor", "serie"]);
            $data["emailEmisor"] = strtolower(trim($data["emailEmisor"]));//Restructuracion de formato de escritura
            $data["correoSecundario"] = strtolower($data["correoSecundario"]);//Restructuracion de formato de escritura
            try {
                $timeData = $request->only(["start_at", "end_at"]);
                $data["start_at"] = Carbon::createFromFormat("H:i:s", $timeData["start_at"]);
                $data["end_at"] = Carbon::createFromFormat("H:i:s", $timeData["end_at"]);
            } catch (Exception $e) {
                Log::error("No se encuentra el tiempo en el documento se creara con el tiempo actual.");
                Log::error($e->getMessage());
                $ahora = Carbon::now();
                $data["start_at"] = $ahora;
                $data["end_at"] = $ahora->addMinute();
            }
            $serieNumero = $data["numSerie"];
            $data["idDocumento"] = $this->getLastIdFromTable();
            $data["estadoWeb"] = "P";
            $fechaEmisionDocumento = $data["fecEmisionDoc"];
            $data["fecEmisionDoc"] = Carbon::createFromFormat("d/m/Y", $data["fecEmisionDoc"]);
            $docPdf = $data["docPdf"];
            $docXML = $data["docXml"];
            $docCdr = $data["docCdr"];
            $fileName = $data["numSerie"];
            try {
                $filePdf = base64_decode($docPdf);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en el documento Impreso" . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            try {
                $fileXml = base64_decode($docXML);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en el archivo XML" . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            try {
                $fileCdr = base64_decode($docCdr);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en la Respuesta CDR." . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            $fechaEmision = explode("/", $fechaEmisionDocumento);
            $dateDirectory = join(DIRECTORY_SEPARATOR, array_reverse($fechaEmision));
            $directory = join(DIRECTORY_SEPARATOR, array($dateDirectory, $data["rucClient"]));
            Storage::makeDirectory($directory);
            $localPath = join(DIRECTORY_SEPARATOR, array($dateDirectory, $data["rucClient"], $fileName));
            $typeFilesystem = config("filesystems.default");
            
            if($data['tipoTransaccion']!='B') Storage::disk($typeFilesystem)->put($localPath . '.pdf', $filePdf);
            Storage::disk($typeFilesystem)->put($localPath . '.xml', $fileXml);
            Storage::disk($typeFilesystem)->put($localPath . '.zip', $fileCdr);
            $data["docPdf"] = $localPath . '.pdf';
            $data["docXml"] = $localPath . '.xml';
            $data["docCdr"] = $localPath . '.zip';
            $datosDocumento = array();
            foreach ($data as $key => $value) {
                $datosDocumento[] = $key . "=>" . $value;
            }
            Log::info('Recibiendo los datos del documento: [' . join(", ", $datosDocumento) . "]");
            $documentoDb = Documento::where("numSerie", $data["numSerie"])->first();
            $token = openssl_random_pseudo_bytes(64);
            $token = bin2hex($token);
            Log::info('Guardando el formato impreso en: ' . $data["docPdf"]);
            Log::info('Guardando el XML del Documento en: ' . $data["docXml"]);
            Log::info('Guardando la Respuesta CDR en: ' . $data["docCdr"]);
            if ($documentoDb && $data['tipoTransaccion']!='B') {
                $data["idDocumento"] = $documentoDb->idDocumento;
                $data["token"] = $token;
                $documentoDb->fill($data)->update();
                $documentoRegistrado = true;
                $documento = $documentoDb;
            } else {
                $data["token"] = $token;
                $documento->fill($data)->save();
                $documentoRegistrado = true;
            }
            Log::info('');
            $documentoAQ = Documento::where("numSerie", $data["numSerie"])->get();
        } catch (Exception $e) {
            Log::error($e->getTraceAsString());
            if ($e instanceof GeneralAPIException) {
                if ($documentoRegistrado) {
                    return response()->json(array("mensaje" => "Se registró existosamente el documento [" . $serieNumero . "] pero: " . $e->getMessage()), 201);
                } else {
                    return response()->json(array("mensaje" => "Error en el documento [" . $serieNumero . "]: " . $e->getMessage()), 400);
                }
            }
            if ($e instanceof Exception) {
                return response()->json(array("mensaje" => " No se pudo registar el documento [" . $serieNumero . "] por: " . $e->getMessage()), 201);
            }
            if (!$mensajeErrorAnexo) {
                return response()->json(array("mensaje" => "No se pudo registar el documento [" . $serieNumero . "] por: " . $mensajeErrorAnexo), 201);
            }
            return response()->json(array("mensaje" => $e->getCode(), "error" => $e->getMessage(), "archivo" => $e->getFile()), 500);
        }
    }

    /*public function publishOnly(Request $request)
    {
        $serieNumero = "";
        $documentoRegistrado = false;
        try {
            $hasher = new Md5Hasher();
            $credentials = array("password" => $hasher->make($request->claveSesion), "nombUsuario" => $request->usuarioSesion);
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Las credenciales proporcionadas para el servicio no son correctas'], 401);
            }
            $dataCliente = $request->only(['direccionClient', 'email', 'estadoCliente', 'nombreClient', 'rucClient', 'rutaImagenClient']);
            $datosCliente = array();
            foreach ($dataCliente as $key => $value) {
                $datosCliente[] = $key . "=>" . $value;
            }
            Log::info('Recibiendo los datos del cliente: [' . join(", ", $datosCliente) . "]");
            $clienteDb = Cliente::find($dataCliente["rucClient"]);
            if (!isset($clienteDb)) {
                $cliente = new Cliente();
                $dataCliente["estadoCliente"] = 1;
                $cliente->fill($dataCliente)->save();
                Log::info('Cliente registrado correctamente');
            } else {
                $dataCliente["estadoCliente"] = 1;
                $clienteDb->fill($dataCliente)->update();
            }
            $mensajeErrorAnexo = false;
            $documento = new Documento();
            $data = $request->only(["numSerie", "fecEmisionDoc", 'estadoSunat', 'estadoWeb', "correoSecundario", 'tipoDoc', "tipoTransaccion", "total", "docPdf", "docXml", "docCdr", "rucClient", "rsRuc", "monedaTransaccion", "emailEmisor", "serie"]);
            $serieNumero = $data["numSerie"];
            $data["idDocumento"] = $this->getLastIdFromTable();
            $data["estadoWeb"] = "P";
            $fechaEmisionDocumento = $data["fecEmisionDoc"];
            $data["fecEmisionDoc"] = Carbon::createFromFormat("d/m/Y", $data["fecEmisionDoc"]);
            $docPdf = $data["docPdf"];
            $docXML = $data["docXml"];
            $docCdr = $data["docCdr"];
            $fileName = $data["numSerie"];
            try {
                $filePdf = base64_decode($docPdf);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en el documento Impreso" . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            try {
                $fileXml = base64_decode($docXML);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en el archivo XML" . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            try {
                $fileCdr = base64_decode($docCdr);
            } catch (Exception $e) {
                $mensajeErrorAnexo = "Error en la Respuesta CDR." . $e->getMessage();
                Log::error($mensajeErrorAnexo);
            }
            $fechaEmision = explode("/", $fechaEmisionDocumento);
            $dateDirectory = join(DIRECTORY_SEPARATOR, array_reverse($fechaEmision));
            $directory = join(DIRECTORY_SEPARATOR, array($dateDirectory, $data["rucClient"]));
            Storage::makeDirectory($directory);
            $localPath = join(DIRECTORY_SEPARATOR, array($dateDirectory, $data["rucClient"], $fileName));
            $typeFilesystem = config("filesystems.default");
            Storage::disk($typeFilesystem)->put($localPath . '.pdf', $filePdf);
            Storage::disk($typeFilesystem)->put($localPath . '.xml', $fileXml);
            Storage::disk($typeFilesystem)->put($localPath . '.zip', $fileCdr);
            $data["docPdf"] = $localPath . '.pdf';
            $data["docXml"] = $localPath . '.xml';
            $data["docCdr"] = $localPath . '.zip';
            $datosDocumento = array();
            foreach ($data as $key => $value) {
                $datosDocumento[] = $key . "=>" . $value;
            }
            Log::info('Recibiendo los datos del documento: [' . join(", ", $datosDocumento) . "]");
            $documentoDb = Documento::where("numSerie", $data["numSerie"])->first();
            $token = openssl_random_pseudo_bytes(64);
            $token = bin2hex($token);
            Log::info('Guardando el formato impreso en: ' . $data["docPdf"]);
            Log::info('Guardando el XML del Documento en: ' . $data["docXml"]);
            Log::info('Guardando la Respuesta CDR en: ' . $data["docCdr"]);
            if ($documentoDb) {
                $data["idDocumento"] = $documentoDb->idDocumento;
                $data["token"] = $token;
                $documentoDb->fill($data)->update();
                $documentoRegistrado = true;
            } else {
                $data["token"] = $token;
                $documento->fill($data)->save();
                $documentoRegistrado = true;
            }
            Log::info('');
            return response()->json(array("mensaje" => "Se registró existosamente el documento [" . $serieNumero . "]"), 201);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            if ($e instanceof Exception) {
                if ($documentoRegistrado) {
                    return response()->json(array("mensaje" => "Se registró existosamente el documento [" . $serieNumero . "] pero: " . $e->getMessage()), 201);
                } else {
                    return response()->json(array("error" => "Error en el documento [" . $serieNumero . "]: " . $e->getMessage()), 400);
                }
            }
            if (!$mensajeErrorAnexo) {
                if ($documentoRegistrado) {
                    return response()->json(array("mensaje" => "Se registró existosamente el documento [" . $serieNumero . "] pero: " . $e->getMessage()), 201);
                } else {
                    return response()->json(array("error" => "Error en el documento [" . $serieNumero . "]: " . $e->getMessage()), 400);
                }
            }
            return response()->json(array("error" => $e->getMessage()), 400);
        }
    }*/

    private function getLastIdFromTable()
    {
        //return substr(hexdec(uniqid(true)), -11, 16);
        $documentoDb = Documento::select('idDocumento')->orderBy('idDocumento','DESC')->first();
        if($documentoDb != null){
            return $documentoDb->idDocumento+1;
        }
        return 1;
    }

    public function registerUser(Cliente $cliente, $rucClient)
    {
        $token = openssl_random_pseudo_bytes(8);
        $password = bin2hex($token);
        $hash = new Md5Hasher();
        $claveUsuario = $hash->make($password);
        $usuarioId = DB::table('fe_usuario')->max('idUsuario');
        $usuario = new Usuario();
        $usuario->fill(["idUsuario" => $usuarioId + 1, "email" => strtolower(trim($cliente->email)), "nombUsuario" => $rucClient,
            "claveUsuario" => $claveUsuario, "estadoUsuario" => "1", "rucClient" => $rucClient, "idRoles" => 3])->save();
        try {
            $usePHPMailer = config('app.use_phpmailer');
            $useGmailApi = env("USE_GMAIL_API", false);
            //dd($usePHPMailer);
            if ($useGmailApi) {
                $controller = new GmailMailerController();
                $controller->sendRegisterEmail($usuario, $password);
            } else if ($usePHPMailer) {
                $documentoController = new PHPMailerController();
                $documentoController->sendRegisterEmail($usuario, $password);
            } else {
                $documentoController = new EmailController();
                $documentoController->sendRegisterEmail($usuario, $password);
            }
        } catch (GeneralAPIException $e) {
            return $e->getMessage();
        }
        return "Mensaje enviado correctamente.";
    }

    public function username()
    {
        return 'nombUsuario';
    }
}
