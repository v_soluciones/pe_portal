<?php

namespace App\Http\Controllers;

use App\Entity\FeGeneralConfig;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use Matriphe\Md5Hash\Md5Hasher;
use Illuminate\Support\Facades\Crypt;
/**
 * Class FeGeneralConfigController
 * @package App\Http\Controllers
 */
class FeGeneralConfigController extends Controller
{

    public function allcorreo()
    {
        $feGeneralConfigs = FeGeneralConfig::all();
        return response()->json($feGeneralConfigs, 200);
      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size = $request->size;
        $order = $request->sort;
        $direction = $request->direction;
        $feGeneralConfigs = FeGeneralConfig::with("correo")->orderBy($order, $direction)->paginate($size);
        return response()->json($feGeneralConfigs, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $feGeneralConfig = new FeGeneralConfig();
        $data = $request->only($feGeneralConfig->getFillable());
        $md5Hasher = new Md5Hasher();
        $cadenaEncriptada = Crypt::encryptString($data["clave"]);
        //$data["clave"] = $md5Hasher->make($data["clave"]);
        $data["clave"] = $cadenaEncriptada;
        $feGeneralConfig->fill($data)->save();
        return response()->json($feGeneralConfig, 201);
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feGeneralConfig = new FeGeneralConfig();
        $data = $request->only($feGeneralConfig->getFillable());
        $hash = new Md5Hasher();
        $data["clave"] = $hash->make($data["clave"]);
        $feGeneralConfig->fill($data)->save();
        return response()->json($feGeneralConfig, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feGeneralConfig = FeGeneralConfig::find($id);
        return view('fe-general-config.show', compact('feGeneralConfig'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feGeneralConfig = FeGeneralConfig::find($id);
        return view('fe-general-config.edit', compact('feGeneralConfig'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  FeGeneralConfig $feGeneralConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeGeneralConfig $feGeneralConfig)
    {
        request()->validate(FeGeneralConfig::$rules);

        $feGeneralConfig->update($request->all());

        return redirect()->route('fe-general-configs.index')
            ->with('success', 'FeGeneralConfig updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $feGeneralConfig = FeGeneralConfig::find($id)->delete();

        return redirect()->route('fe-general-configs.index')
            ->with('success', 'FeGeneralConfig deleted successfully');
    }
}
