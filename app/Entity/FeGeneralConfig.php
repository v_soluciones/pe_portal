<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FeGeneralConfig
 *
 * @property $id
 * @property $fecha
 * @property $configuracion
 * @property $correo
 * @property $clave
 * @property $puerto
 * @property $host
 * @property $smtp
 * @property $encriptado
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class FeGeneralConfig extends Model
{
    //use SoftDeletes;

    protected $table = 'fe_general_config';
    protected $primaryKey = 'id';
    protected $fillable = ['fecha','configuracion','correo','clave','puerto','host','smtp','encriptado','secret'];
    protected $casts = ['rucClient' => 'string'];

    public $timestamps = false;
    
    static $rules = [
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   



}
