<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('fe_general_config')) {
            Schema::create('fe_general_config', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->timestamp('inicio')->useCurrent();
				$table->timestamp('final')->useCurrent();
                $table->timestamp('deleted_at')->useCurrent();
                $table->longText("configuracion")->nullable();
                $table->text("correo")->nullable();
                $table->text("clave")->nullable();
                $table->text("puerto")->nullable();
                $table->text("host")->nullable();
                $table->text("smtp")->nullable();
				$table->text("secret")->nullable();
                $table->text("encriptado")->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fe_general_config');
    }
}
