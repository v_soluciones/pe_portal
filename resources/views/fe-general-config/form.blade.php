<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('fecha') }}
            {{ Form::text('fecha', $feGeneralConfig->fecha, ['class' => 'form-control' . ($errors->has('fecha') ? ' is-invalid' : ''), 'placeholder' => 'Fecha']) }}
            {!! $errors->first('fecha', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('configuracion') }}
            {{ Form::text('configuracion', $feGeneralConfig->configuracion, ['class' => 'form-control' . ($errors->has('configuracion') ? ' is-invalid' : ''), 'placeholder' => 'Configuracion']) }}
            {!! $errors->first('configuracion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('correo') }}
            {{ Form::text('correo', $feGeneralConfig->correo, ['class' => 'form-control' . ($errors->has('correo') ? ' is-invalid' : ''), 'placeholder' => 'Correo']) }}
            {!! $errors->first('correo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('clave') }}
            {{ Form::text('clave', $feGeneralConfig->clave, ['class' => 'form-control' . ($errors->has('clave') ? ' is-invalid' : ''), 'placeholder' => 'Clave']) }}
            {!! $errors->first('clave', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('puerto') }}
            {{ Form::text('puerto', $feGeneralConfig->puerto, ['class' => 'form-control' . ($errors->has('puerto') ? ' is-invalid' : ''), 'placeholder' => 'Puerto']) }}
            {!! $errors->first('puerto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('host') }}
            {{ Form::text('host', $feGeneralConfig->host, ['class' => 'form-control' . ($errors->has('host') ? ' is-invalid' : ''), 'placeholder' => 'Host']) }}
            {!! $errors->first('host', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('smtp') }}
            {{ Form::text('smtp', $feGeneralConfig->smtp, ['class' => 'form-control' . ($errors->has('smtp') ? ' is-invalid' : ''), 'placeholder' => 'Smtp']) }}
            {!! $errors->first('smtp', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('encriptado') }}
            {{ Form::text('encriptado', $feGeneralConfig->encriptado, ['class' => 'form-control' . ($errors->has('encriptado') ? ' is-invalid' : ''), 'placeholder' => 'Encriptado']) }}
            {!! $errors->first('encriptado', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>