@extends('layouts.app')

@section('template_title')
    Fe General Config
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Fe General Config') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('fe-general-configs.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Fecha</th>
										<th>Configuracion</th>
										<th>Correo</th>
										<th>Clave</th>
										<th>Puerto</th>
										<th>Host</th>
										<th>Smtp</th>
										<th>Encriptado</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($feGeneralConfigs as $feGeneralConfig)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $feGeneralConfig->fecha }}</td>
											<td>{{ $feGeneralConfig->configuracion }}</td>
											<td>{{ $feGeneralConfig->correo }}</td>
											<td>{{ $feGeneralConfig->clave }}</td>
											<td>{{ $feGeneralConfig->puerto }}</td>
											<td>{{ $feGeneralConfig->host }}</td>
											<td>{{ $feGeneralConfig->smtp }}</td>
											<td>{{ $feGeneralConfig->encriptado }}</td>

                                            <td>
                                                <form action="{{ route('fe-general-configs.destroy',$feGeneralConfig->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('fe-general-configs.show',$feGeneralConfig->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('fe-general-configs.edit',$feGeneralConfig->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $feGeneralConfigs->links() !!}
            </div>
        </div>
    </div>
@endsection
