@extends('layouts.app')

@section('template_title')
    {{ $feGeneralConfig->name ?? 'Show Fe General Config' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Fe General Config</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('fe-general-configs.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Fecha:</strong>
                            {{ $feGeneralConfig->fecha }}
                        </div>
                        <div class="form-group">
                            <strong>Configuracion:</strong>
                            {{ $feGeneralConfig->configuracion }}
                        </div>
                        <div class="form-group">
                            <strong>Correo:</strong>
                            {{ $feGeneralConfig->correo }}
                        </div>
                        <div class="form-group">
                            <strong>Clave:</strong>
                            {{ $feGeneralConfig->clave }}
                        </div>
                        <div class="form-group">
                            <strong>Puerto:</strong>
                            {{ $feGeneralConfig->puerto }}
                        </div>
                        <div class="form-group">
                            <strong>Host:</strong>
                            {{ $feGeneralConfig->host }}
                        </div>
                        <div class="form-group">
                            <strong>Smtp:</strong>
                            {{ $feGeneralConfig->smtp }}
                        </div>
                        <div class="form-group">
                            <strong>Encriptado:</strong>
                            {{ $feGeneralConfig->encriptado }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
